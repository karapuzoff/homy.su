<?php
class faq {
  /**
   *
   * Construct FAQ page
   *
   * @return   integer  html code with faq items
   *
   */
   public function index() {
      $content  = new template();
      $db = new mysqlcrud();
      $db->connect();

      //
      // Получаем список вопросов и ответов
      //
      $faq_list = '';

      $db->sql('
         SELECT
            faq_questions.id,
            faq_questions.category_id,
            faq_categories.title,
            faq_questions.question,
            faq_questions.answer,
            faq_questions.mtime
         FROM faq_questions
         LEFT JOIN
            faq_categories
         ON
            faq_categories.category_id = faq_questions.category_id
         ORDER BY
            faq_categories.category_id
      ');
      $res = $db->getResult();

      if (count($res) > 0) {
         foreach ($res as $val) {
            $format_list[$val['category_id']]['title'] = $val['title'];
            $list['faq_number']   = $val['id'];
            $list['faq_question'] = $val['question'];
            $list['faq_answer']   = $val['answer'];
            @$format_list[$val['category_id']]['list'] .= $content->design('faq','list',$list);
         }
         foreach ($format_list as $val) {
            $faq_list .= $content->design('faq','category',$val);
         }
      }

      //
      // Получаем время последней модификации листа ЧаВО
      //
      $db->select('faq_questions','mtime',null,null,'mtime DESC',1);
      $res = $db->getResult();

      $params = new TimeParams();
      $params->format = 'd F Y года в H:i';
      $params->monthInflected = true;
      $params->date = $res[0]['mtime'];
      $mtime = RUtils::dt()->ruStrFTime($params);

      // START
      // Задаем meta заголовки страницы
      $header['description'] = 'Страница с часто задаваемыми вопросами и ответами на них';
      $header['keywords'] = 'faq, чаво, вопросы, ответы, решения';
      $header['title'] = 'ЧаВО';
      echo $content->design('index','header',$header);

		// Подключаем логотип, форму поиска и корзину покупок
      $header = new header();

      $faq['mtime'] = $mtime;
      $faq['faq_body'] = $faq_list;
      echo $content->design('faq','index',$faq);
   }
}
