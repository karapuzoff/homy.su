<?php
class ntree {
  /**
   *
   * Черновой вариант построения дерева категорий для прайс-листа
   *
   */
   public function index() {
      $db = new mysqlcrud();
      $db->connect();

      $ntree = new nestedtree();

      //new dBug(add_subnode("4.5.2",17));
      //new dBug(del_node_all(2));
      //new dBug(del_node(2));
      //new dBug(rename_node("Бытовая техника",1));
      //new dBug(single_path(19));

      //$res = $ntree->show_tree();
      //new dBug($res);
      //$res = $ntree->add_subnode("Прочее",1);

      $array = array(
         'Крупаная бытовая техника' => array(
            'Холодильники',
            'Морозильники',
            'Лари',
            'Стиральные машины',
            'Посудомоечные машины',
            'Плиты',
            'Вытяжки',
            'Кулеры',
            'Встраиваемые духовые шкафы',
            'Встраиваемые варочные панели',
         ),
         'Аудио-видео' => array(
            'Телевизоры',
            'Цифровые приставки для ТВ',
            'DVD',
            'Музыкальные центры и бумбоксы',
            'Радиоприемники',
            'Автотехника' => array(
               'Авторадио',
               'Автоколонки',
               'Видеорегистраторы',
               'Радар-детекторы',
               'Автокомпрессоры',
               'Автохолодильники и аккумуляторы холода'
            ),
            'Сопутствующее оборудование и аксессуары' => array(
               'Антенны',
               'Кронштейны',
               'Удлинители, сетевые фильтры',
               'Кабели, разъемы',
               'Блоки питания',
               'Наушники и гарнитуры',
               'Микрофоны'
            )
         ),
         'Техника для дома' => array(
            'Климатическая техника' => array(
               'Вентиляторы',
               'Обогреватели',
               'Кондиционеры',
               'Водонагреватели',
               'Увлажнители и очистители воздуха'
            ),
            'Техника для уборки' => array(
               'Пароочистители',
               'Пылесосы'
            ),
            'Техника для ухода за одеждой' => array(
               'Утюги',
               'Отпариватели для одежды',
               'Гладильные доски',
               'Сушилки для белья',
               'Сушилки для обуви',
               'Швейные машинки'
            ),
            'Светотехника' => array(
               'Энергосберегающие и LED лампы',
               'Праздничный свет',
               'Гирлянды'
            ),
            'Телефоны' => array(
               'Радиотелефоны',
               'Проводные телефоны',
               'Аксессуары к телефонам'
            ),
            'Электроинструмент' => array(
               'Дрели и шуруповерты',
               'Перфораторы',
               'Шлифовальные машины',
               'Лобзики',
               'Пилы',
               'Сварочные аппараты',
               'Прочий электроинструмент'
            ),
            'Часы'
         ),
         'Техника для кухни' => array(
            'Печи и тостеры' => array(
               'Микроволновые печи',
               'Хлебопечи',
               'Духовые шкафы',
               'Тостеры, ростеры',
               'Сэндвичницы и вафельницы',
               'Аксессуары для печей'
            ),
            'Приготовление блюд' => array(
               'Аэрогрили',
               'Мультиварки и чаши',
               'Пароварки',
               'Фритюрницы',
               'Электросковороды и электрошашлычницы',
               'Плитки настольные'
            ),
            'Приготовление напитков' => array(
               'Электрочайники, термопоты, самовары',
               'Кофеварки и кофемашины',
               'Кофемолки',
               'Соковыжималки'
            ),
            'Измельчение и смешивание' => array(
               'Мясорубки',
               'Миксеры',
               'Блендеры',
               'Кухонные комбайны и измельчители',
               'Ломтерезки',
               'Аксессуары для мясорубок'
            ),
            'Сушилки для овощей, фруктов, грибов',
            'Весы кухонные'
         ),
         'Посуда и аксессуары' => array(
            'Наборы посуды и кастрюли',
            'Кухонные наборы',
            'Кухонная утварь'
         ),
         'Красота и здоровье' => array(
            'Фены и приборы для укладки',
            'Машинки для стрижки',
            'Электробритвы и тримеры',
            'Напольные весы',
            'Велосипеды'
         )
      );/**/

      /* Очищаем таблицу категорий до корневой директории */
      $db->select('category','MAX(cat_id)');
      $sql = $db->getResult();
      $st = 2;
      $en = $sql[0]['MAX(cat_id)'];

      for ($i=$st;$i<=$en;$i++) {
         $res = $ntree->del_node_all($i);
      }
      $db->sql('ALTER TABLE category auto_increment = 2');
      $sql = $db->getResult();
      /**/

      /* Заполняем из массива (временное решение) */
      foreach ($array as $cat_name => $cat) {
         echo $cat_name . '<br />';

         if ($this->catCheck($cat_name)) {
            $ntree->add_subnode($cat_name,1);
            $last_id['cat'] = $this->lastID();
         }

         foreach ($cat as $subcat_name => $subcat) {
            //echo '['.$last_id['cat'].'] ';

            if (is_array($subcat)) {
               echo '--- ' . $subcat_name . '<br />';

               if ($this->catCheck($subcat_name)) {
                  $ntree->add_subnode($subcat_name,$last_id['cat']);
                  $last_id['subcat'] = $this->lastID();
               }

               foreach ($subcat as $subsubcat) {
                  //echo '['.$last_id['subcat'].'] ';
                  echo '--- --- ' . $subsubcat . '<br />';

                  if ($this->catCheck($subsubcat)) {
                     $ntree->add_subnode($subsubcat,$last_id['subcat']);
                  }
               }
            } else {
               echo '--- ' . $subcat . '<br />';

               if ($this->catCheck($subcat)) {
                  $ntree->add_subnode($subcat,$last_id['cat']);
               }
            }

         }
      }
      $this->translit();/**/
   }

   public function translit() {
      $db = new mysqlcrud();
      $db->connect();

      $db->select('category','*',NULL,'cat_id!=1');
      $result = $db->getResult();

      foreach ($result as $cat) {
         //new dBug($cat);
         $update['url'] = translit::translify($cat['name']);
         $pattern = array('`',',');
         $update['url'] = str_replace($pattern,'',$update['url']);
         $update['url'] = str_replace(' ','-',$update['url']);
         $update['url'] = strtolower($update['url']);

         $db->update('category',$update,'cat_id='.$cat['cat_id']);
      }
   }

   public function add() {
      $ntree = new nestedtree();
      new dBug($ntree->show_tree());
      //new dBug($ntree->add_subnode("Велосипеды"));
      //new dBug($ntree->del_node(79));
   }

   private function catCheck($cat_name) {
      $db = new mysqlcrud();
      $db->connect();

      $db->select('category','*',null,'name="'.$cat_name.'"');
      $mysql = $db->getResult();

      if (count($mysql) == 0)
         return true;
      else
         return false;
   }

   private function lastID() {
      $db = new mysqlcrud();
      $db->connect();

      $db->sql('SELECT LAST_INSERT_ID()');
      $sql = $db->getResult();

      $last_id = $sql[0]['LAST_INSERT_ID()'];

      return $last_id;
   }
}
