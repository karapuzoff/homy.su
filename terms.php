<?php

use \template\template;
use \tmp\master;
use \template\header;

class terms {
   /**
   * Правила пользования магазином
   * @return   integer  html code
   */
   public function index() {
      $content  = new template();

      // START
      // Задаем meta заголовки страницы
      $header['description'] = 'Правила пользования магазином';
      $header['keywords'] = 'правила, terms';
      $header['title'] = 'Оплата, доставка и получение товара'.HEAD_TITLE_END;
      echo $content->design('index','header',$header);

		// Подключаем логотип, форму поиска и корзину покупок
      $header = new header();

      $terms['path'] = 'http://static.homy.su/zakon/';
      echo $content->design('terms','index',$terms);
   }
}
