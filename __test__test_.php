<?php
use \template\template;
use \helper\generate;
use \helper\date;
use \order\order;
use \db\mysql\mysqlcrud;
use \debug\dBug;
use \core\sitemap;
use \tmp\master;
use \product\prod;
use \tmp\nestedtree;

use \auth\extauth;

class __test__test_ {

   public function __construct() {
      $this->db = new mysqlcrud();
      $this->db->connect();
      $this->content = new template();
      $this->ntree = new nestedtree();
      /*$this->telegram = new telegram();

      $this->search = 'Meizu M3s Mini 16Gb';

      $this->chat_id = '61564446';

      //$this->telegram->token = cfg::BOT_ID.':'.cfg::BOT_TOKEN;/**/
   }

   function phpinfo() { phpinfo(); }

   function tree() {
      //new dBug($this->ntree->add_subnode('Самокаты',106));
      //new dBug($this->ntree->del_node(114));
   }

	function cart() {
		$cart = $_SESSION['shoppingcart'];
		//new dBug($cart);

		//$elements = [];

		foreach ($cart as $id => $array) {
			$this->db->sql('
	         SELECT
	            catalog.cat_id,
					catalog.name,
					catalog.price,
					brands.brand_clean AS brand
	         FROM catalog
				LEFT JOIN
					brands
				ON
					catalog.brand_id = brands.brand_id
	         WHERE
	            1c_id = '.$id
	      );
			$product = $this->db->getResult();

			$data = $product[0];
			$data['brand']		= mb_strtoupper($data['brand'], 'utf-8');
			$data['name']		= $data['brand'] . ' ' . $data['name'];
			$data['category'] = prod::categoryPath($data['cat_id']);

			$elements[] = json_encode([
				'id' => $id,
				"name" 		=> $data['name'],
				"price" 		=> $data['price'],
				"brand"		=> $data['brand'],
				"category" 	=> $data['category'],
				"quantity" 	=> $array['qty']
			], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
		}

		$push = implode(',',$elements);

		echo $push;
	}

   function sitemap() {
      set_time_limit(0);
      $sitemap = new sitemap();

      //игнорировать ссылки с расширениями:
      $sitemap->set_ignore(array("javascript:", ".css", ".js", ".ico", ".jpg", ".png", ".jpeg", ".swf", ".gif"));

      //ссылка Вашего сайта:
      $sitemap->get_links("http://homy.su");

      //если нужно вернуть просто массив с данными:
      $arr = $sitemap->get_array();
      new dBug($arr);
      //echo "<pre>";
      //print_r($arr);
      //echo "</pre>";

      //header ("content-type: text/xml");
      $map = $sitemap->generate_sitemap();
      //echo $map;
   }

   function extauth() {
      $bt = new extauth();
      $bt->buttons();
   }

   function auth_twitter() {
      define('CONSUMER_KEY', 'hOvTGKCnTl72KTxfhubSMKbnm');
      define('CONSUMER_SECRET', 'zvPzCHkHw6OO5vVIRzzlCVByqbLDOK7MHp2B1bxYQJHXYxhDvl');

      define('REQUEST_TOKEN_URL', 'https://api.twitter.com/oauth/request_token');
      define('AUTHORIZE_URL', 'https://api.twitter.com/oauth/authorize');
      define('ACCESS_TOKEN_URL', 'https://api.twitter.com/oauth/access_token');
      define('ACCOUNT_DATA_URL', 'https://api.twitter.com/1.1/users/show.json');

      define('CALLBACK_URL', 'http://mba.lan/__test__test_/auth_twitter');

      // формируем подпись для получения токена доступа
      define('URL_SEPARATOR', '&');

      $oauth_nonce = md5(uniqid(rand(), true));
      $oauth_timestamp = time();

      $params = array(
          'oauth_callback=' . urlencode(CALLBACK_URL) . URL_SEPARATOR,
          'oauth_consumer_key=' . CONSUMER_KEY . URL_SEPARATOR,
          'oauth_nonce=' . $oauth_nonce . URL_SEPARATOR,
          'oauth_signature_method=HMAC-SHA1' . URL_SEPARATOR,
          'oauth_timestamp=' . $oauth_timestamp . URL_SEPARATOR,
          'oauth_version=1.0'
      );

      $oauth_base_text = implode('', array_map('urlencode', $params));
      $key = CONSUMER_SECRET . URL_SEPARATOR;
      $oauth_base_text = 'GET' . URL_SEPARATOR . urlencode(REQUEST_TOKEN_URL) . URL_SEPARATOR . $oauth_base_text;
      $oauth_signature = base64_encode(hash_hmac('sha1', $oauth_base_text, $key, true));


      // получаем токен запроса
      $params = array(
          URL_SEPARATOR . 'oauth_consumer_key=' . CONSUMER_KEY,
          'oauth_nonce=' . $oauth_nonce,
          'oauth_signature=' . urlencode($oauth_signature),
          'oauth_signature_method=HMAC-SHA1',
          'oauth_timestamp=' . $oauth_timestamp,
          'oauth_version=1.0'
      );
      $url = REQUEST_TOKEN_URL . '?oauth_callback=' . urlencode(CALLBACK_URL) . implode('&', $params);

      $response = file_get_contents($url);
      parse_str($response, $response);

      $oauth_token = $response['oauth_token'];
      $oauth_token_secret = $response['oauth_token_secret'];


      // генерируем ссылку аутентификации

      $link = AUTHORIZE_URL . '?oauth_token=' . $oauth_token;

      echo '<a href="' . $link . '">Аутентификация через Twitter</a>';


      if (!empty($_GET['oauth_token']) && !empty($_GET['oauth_verifier'])) {
          // готовим подпись для получения токена доступа

          $oauth_nonce = md5(uniqid(rand(), true));
          $oauth_timestamp = time();
          $oauth_token = $_GET['oauth_token'];
          $oauth_verifier = $_GET['oauth_verifier'];


          $oauth_base_text = "GET&";
          $oauth_base_text .= urlencode(ACCESS_TOKEN_URL)."&";

          $params = array(
              'oauth_consumer_key=' . CONSUMER_KEY . URL_SEPARATOR,
              'oauth_nonce=' . $oauth_nonce . URL_SEPARATOR,
              'oauth_signature_method=HMAC-SHA1' . URL_SEPARATOR,
              'oauth_token=' . $oauth_token . URL_SEPARATOR,
              'oauth_timestamp=' . $oauth_timestamp . URL_SEPARATOR,
              'oauth_verifier=' . $oauth_verifier . URL_SEPARATOR,
              'oauth_version=1.0'
          );

          $key = CONSUMER_SECRET . URL_SEPARATOR . $oauth_token_secret;
          $oauth_base_text = 'GET' . URL_SEPARATOR . urlencode(ACCESS_TOKEN_URL) . URL_SEPARATOR . implode('', array_map('urlencode', $params));
          $oauth_signature = base64_encode(hash_hmac("sha1", $oauth_base_text, $key, true));

          // получаем токен доступа
          $params = array(
              'oauth_nonce=' . $oauth_nonce,
              'oauth_signature_method=HMAC-SHA1',
              'oauth_timestamp=' . $oauth_timestamp,
              'oauth_consumer_key=' . CONSUMER_KEY,
              'oauth_token=' . urlencode($oauth_token),
              'oauth_verifier=' . urlencode($oauth_verifier),
              'oauth_signature=' . urlencode($oauth_signature),
              'oauth_version=1.0'
          );
          $url = ACCESS_TOKEN_URL . '?' . implode('&', $params);

          $response = file_get_contents($url);
          parse_str($response, $response);


          // формируем подпись для следующего запроса
          $oauth_nonce = md5(uniqid(rand(), true));
          $oauth_timestamp = time();

          $oauth_token = $response['oauth_token'];
          $oauth_token_secret = $response['oauth_token_secret'];
          $screen_name = $response['screen_name'];

          $params = array(
              'oauth_consumer_key=' . CONSUMER_KEY . URL_SEPARATOR,
              'oauth_nonce=' . $oauth_nonce . URL_SEPARATOR,
              'oauth_signature_method=HMAC-SHA1' . URL_SEPARATOR,
              'oauth_timestamp=' . $oauth_timestamp . URL_SEPARATOR,
              'oauth_token=' . $oauth_token . URL_SEPARATOR,
              'oauth_version=1.0' . URL_SEPARATOR,
              'screen_name=' . $screen_name
          );
          $oauth_base_text = 'GET' . URL_SEPARATOR . urlencode(ACCOUNT_DATA_URL) . URL_SEPARATOR . implode('', array_map('urlencode', $params));

          $key = CONSUMER_SECRET . '&' . $oauth_token_secret;
          $signature = base64_encode(hash_hmac("sha1", $oauth_base_text, $key, true));

      	// получаем данные о пользователе
          $params = array(
              'oauth_consumer_key=' . CONSUMER_KEY,
              'oauth_nonce=' . $oauth_nonce,
              'oauth_signature=' . urlencode($signature),
              'oauth_signature_method=HMAC-SHA1',
              'oauth_timestamp=' . $oauth_timestamp,
              'oauth_token=' . urlencode($oauth_token),
              'oauth_version=1.0',
              'screen_name=' . $screen_name
          );

          $url = ACCOUNT_DATA_URL . '?' . implode(URL_SEPARATOR, $params);

          $response = file_get_contents($url);
          $user_data = json_decode($response, true);

          new dBug($user_data);
      }
   }

   function auth_yandex() {
      $client_id = '9fbee3ce38874a76877a4b3770ad5a0c'; // Id приложения
      $client_secret = '0f79f17c506e4d34bbaa3163be38e1b2'; // Пароль приложения
      $redirect_uri = 'http://localhost/yandex-auth'; // Callback URI

      $url = 'https://oauth.yandex.ru/authorize';

      $params = array(
          'response_type' => 'code',
          'client_id'     => $client_id,
          'display'       => 'popup'
      );

      echo $link = '<p><a href="' . $url . '?' . urldecode(http_build_query($params)) . '">Аутентификация через Yandex</a></p>';

      if (isset($_GET['code'])) {
          $result = false;

          $params = array(
              'grant_type'    => 'authorization_code',
              'code'          => $_GET['code'],
              'client_id'     => $client_id,
              'client_secret' => $client_secret
          );

          $url = 'https://oauth.yandex.ru/token';

          $curl = curl_init();
          curl_setopt($curl, CURLOPT_URL, $url);
          curl_setopt($curl, CURLOPT_POST, 1);
          curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
          $result = curl_exec($curl);
          curl_close($curl);

          $tokenInfo = json_decode($result, true);

          if (isset($tokenInfo['access_token'])) {
              $params = array(
                  'format'       => 'json',
                  'oauth_token'  => $tokenInfo['access_token']
              );

              $userInfo = json_decode(file_get_contents('https://login.yandex.ru/info' . '?' . urldecode(http_build_query($params))), true);
              if (isset($userInfo['id'])) {
                  $userInfo = $userInfo;
                  $result = true;
              }
          }

          if ($result) {
             new dBug($userInfo);
              /*echo "Социальный ID пользователя: " . $userInfo['id'] . '<br />';
              echo "Имя пользователя: " . $userInfo['real_name'] . '<br />';
              echo "Email: " . $userInfo['default_email'] . '<br />';
              echo "Пол пользователя: " . $userInfo['sex'] . '<br />';
              echo "День Рождения: " . $userInfo['birthday'] . '<br />';/**/
          }
      }
   }

   function auth_google() {
      $client_id = '665596563689-gp6mf1oqp8hmujo007bdnocaaeiic6js.apps.googleusercontent.com'; // Client ID
      $client_secret = 'PkgAHVm2wykKBABKlfFn0dnm'; // Client secret
      $redirect_uri = 'http://localhost/__test__test_/auth_google'; // Redirect URIs

      $url = 'https://accounts.google.com/o/oauth2/auth';

      $params = array(
          'redirect_uri'  => $redirect_uri,
          'response_type' => 'code',
          'client_id'     => $client_id,
          'scope'         => 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile'
      );

      echo $link = '<p><a href="' . $url . '?' . urldecode(http_build_query($params)) . '">Аутентификация через Google</a></p>';

      if (isset($_GET['code'])) {
          $result = false;

          $params = array(
              'client_id'     => $client_id,
              'client_secret' => $client_secret,
              'redirect_uri'  => $redirect_uri,
              'grant_type'    => 'authorization_code',
              'code'          => $_GET['code']
          );

          $url = 'https://accounts.google.com/o/oauth2/token';

          $curl = curl_init();
          curl_setopt($curl, CURLOPT_URL, $url);
          curl_setopt($curl, CURLOPT_POST, 1);
          curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
          $result = curl_exec($curl);
          curl_close($curl);
          $tokenInfo = json_decode($result, true);

          if (isset($tokenInfo['access_token'])) {
              $params['access_token'] = $tokenInfo['access_token'];

              $userInfo = json_decode(file_get_contents('https://www.googleapis.com/oauth2/v1/userinfo' . '?' . urldecode(http_build_query($params))), true);
              if (isset($userInfo['id'])) {
                  $userInfo = $userInfo;
                  $result = true;
              }
          }

          if ($result) {
             new dBug($userInfo);
              /*echo "Социальный ID пользователя: " . $userInfo['id'] . '<br />';
              echo "Имя пользователя: " . $userInfo['name'] . '<br />';
              echo "Email: " . $userInfo['email'] . '<br />';
              echo "Ссылка на профиль пользователя: " . $userInfo['link'] . '<br />';
              echo "Пол пользователя: " . $userInfo['gender'] . '<br />';
              echo '<img src="' . $userInfo['picture'] . '" />'; echo "<br />";/**/
          }

      }
   }
   function auth_fb() {
      $client_id = '198473150564709'; // Client ID
      $client_secret = 'e38932541461a68d998cbb37a20d38eb'; // Client secret
      $redirect_uri = 'http://mba.lan/__test__test_/auth_fb'; // Redirect URIs

      $url = 'https://www.facebook.com/dialog/oauth';

      $params = array(
          'client_id'     => $client_id,
          'redirect_uri'  => $redirect_uri,
          'response_type' => 'code',
          'scope'         => 'email,user_birthday'
      );

      echo $link = '<p><a href="' . $url . '?' . urldecode(http_build_query($params)) . '">Аутентификация через Facebook</a></p>';

      if (isset($_GET['code'])) {
          $result = false;

          $params = array(
              'client_id'     => $client_id,
              'redirect_uri'  => $redirect_uri,
              'client_secret' => $client_secret,
              'code'          => $_GET['code']
          );

          $url = 'https://graph.facebook.com/oauth/access_token';

          $tokenInfo = null;
          parse_str(file_get_contents($url . '?' . http_build_query($params)), $tokenInfo);

          if (count($tokenInfo) > 0 && isset($tokenInfo['access_token'])) {
              $params = array('access_token' => $tokenInfo['access_token']);

              $userInfo = json_decode(file_get_contents('https://graph.facebook.com/me' . '?' . urldecode(http_build_query($params))), true);

              if (isset($userInfo['id'])) {
                  $userInfo = $userInfo;
                  $result = true;
              }
          }

          if ($result) {
             new dBug($userInfo);
              /*echo "Социальный ID пользователя: " . $userInfo['id'] . '<br />';
              echo "Имя пользователя: " . $userInfo['name'] . '<br />';
              echo "Email: " . $userInfo['email'] . '<br />';
              echo "Ссылка на профиль пользователя: " . $userInfo['link'] . '<br />';
              echo "Пол пользователя: " . $userInfo['gender'] . '<br />';
              echo "ДР: " . $userInfo['birthday'] . '<br />';
              echo '<img src="http://graph.facebook.com/' . $userInfo['username'] . '/picture?type=large" />'; echo "<br />";/**/
          }

      }
   }
   function auth_vk() {
          $client_id = '4778198'; // ID приложения
          $client_secret = 'RG0l6BhjFMfdkupimXmK'; // Защищённый ключ
          $redirect_uri = 'http://mba.lan/__test__test_/auth_vk'; // Адрес сайта

          $url = 'http://oauth.vk.com/authorize';

          $params = array(
              'client_id'     => $client_id,
              'redirect_uri'  => $redirect_uri,
              'response_type' => 'code'
          );

          echo $link = '<p><a href="' . $url . '?' . urldecode(http_build_query($params)) . '">Аутентификация через ВКонтакте</a></p>';

      if (isset($_GET['code'])) {
          $result = false;
          $params = array(
              'client_id' => $client_id,
              'client_secret' => $client_secret,
              'code' => $_GET['code'],
              'redirect_uri' => $redirect_uri
          );

          $token = json_decode(file_get_contents('https://oauth.vk.com/access_token' . '?' . urldecode(http_build_query($params))), true);

          if (isset($token['access_token'])) {
              $params = array(
                  'uids'         => $token['user_id'],
                  'fields'       => 'uid,first_name,last_name,screen_name,sex,bdate,photo_big',
                  'access_token' => $token['access_token']
              );

              $userInfo = json_decode(file_get_contents('https://api.vk.com/method/users.get' . '?' . urldecode(http_build_query($params))), true);
              if (isset($userInfo['response'][0]['uid'])) {
                  $userInfo = $userInfo['response'][0];
                  $result = true;
              }
          }

          if ($result) {
             new dBug($userInfo);
              /*echo "Социальный ID пользователя: " . $userInfo['uid'] . '<br />';
              echo "Имя пользователя: " . $userInfo['first_name'] . '<br />';
              echo "Ссылка на профиль пользователя: " . $userInfo['screen_name'] . '<br />';
              echo "Пол пользователя: " . $userInfo['sex'] . '<br />';
              echo "День Рождения: " . $userInfo['bdate'] . '<br />';
              echo '<img src="' . $userInfo['photo_big'] . '" />'; echo "<br />";/**/
          }
      }
   }

   function login() {
      echo $this->content->design('ulogin','script');
   }
   function ulogin() {
      $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
      $user = json_decode($s, true);
      //$user['network'] - соц. сеть, через которую авторизовался пользователь
      //$user['identity'] - уникальная строка определяющая конкретного пользователя соц. сети
      //$user['first_name'] - имя пользователя
      //$user['last_name'] - фамилия пользователя
      new dBug($user);

   }

   public function todoist() {
      echo 1;

      //echo generate::pin();
      /*$this->todoist = new vendor\todoist\todoist();
      $todo = [
         'order' => generate::orderID(),
         'fio'   => 'Тарасов Иван',
         'phone' => '+79155173925'
      ];
      $todo = $this->todoist->add($todo);

      //echo $request['temp_id_mapping'][$temp_id];
      new dBug($todo);

      //$result = json_decode($result,true);
      //new dBug($result);
      /**/
   }

   public function telegram() {
      echo 'Утанавливаю хук...';
      $this->telegram->setWebHook('https://'.cfg::BOT_WEBHOOK_K.cfg::BOT_ID);

      /*$message = 'Привет! Идентификатор этого чата *'.$this->chat_id.'*';

      $output = $this->telegram->apiRequest("sendMessage", [
         'chat_id'      => $this->chat_id,
         'text'         => $message,
         'parse_mode'   => 'Markdown',
         'reply_markup' => $this->telegram->ReplyKeyboardHide()
      ]);
      new dBug($output);/**/

      /*$post = '{"update_id":558250566,"message":{"message_id":23,"from":{"id":61564446,"first_name":"Ivan","last_name":"Karapuzoff","username":"karapuzoff"},"chat":{"id":61564446,"first_name":"Ivan","last_name":"Karapuzoff","username":"karapuzoff","type":"private"},"date":1470898837,"contact":{"phone_number":"79155173925","first_name":"Ivan","last_name":"Karapuzoff","user_id":61564446}}}';
      $output = json_decode($post, TRUE);
      new dBug($output);/**/

      /*$reply_markup = [
         'keyboard' => [
            [
               [
                  'text' => 'Авторизация',
                  'request_contact' => true
               ]
            ]
         ],
         'one_time_keyboard' => true,
         'resize_keyboard' => true
      ];
      $reply_markup = json_encode($reply_markup);
      $reply = $this->telegram->apiRequest("sendMessage", array(
         'chat_id' => $this->chat_id,
         'text' => 'Авторизуйтесь с помощью телефона',
         'reply_markup' => $reply_markup
      ));
      new dBug($reply);
      /**/
   }

   public function nested() {
      $nes = new nestedtree();
      //$nes->add_subnode('Смартфоны',55);
   }

   public function auth() {
      /*if (!isset($_SERVER['PHP_AUTH_USER']) && $_SERVER['PHP_AUTH_USER'] != cfg::YML_LOGIN && $_SERVER['PHP_AUTH_PW'] != cfg::YML_PASSWORD) {
         header('WWW-Authenticate: Basic realm="Are you realy Yandex.Market Bot?"');
         header('HTTP/1.0 401 Unauthorized');
         echo 'Нечего тут делать, правда.';
         exit;
      }

      echo "<p>Hello {$_SERVER['PHP_AUTH_USER']}.</p>";
      echo "<p>Вы ввели пароль {$_SERVER['PHP_AUTH_PW']}.</p>";/**/




      $valid_passwords = array (cfg::YML_LOGIN => cfg::YML_PASSWORD);
      $valid_users = array_keys($valid_passwords);

      $user = $_SERVER['PHP_AUTH_USER'];
      $pass = $_SERVER['PHP_AUTH_PW'];

      $validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);

      if (!$validated) {
         header('WWW-Authenticate: Basic realm="My Realm"');
         header('HTTP/1.0 401 Unauthorized');
         die ("Not authorized");
      }

      // If arrives here, is a valid user.
      echo "<p>Welcome $user.</p>";
      echo "<p>Congratulation, you are into the system.</p>";
   }

   /**
   * Формирование YML прайс-листа для Яндекс.Маркета
   * @author   Ivan Karapuzoff <ivan@karapuzoff.net>
   * @version  2.1
   */
   public function yamarketPrice() {
      $blacklist = '56197,29927,29921,26557,54154,43732,45640,31229,31230,45244,45760,42094,51808,45428,45239,56713,45859,40014,28589';

      ### 1 - заголовки и информацио о магазине
      $yml['@attributes'] = array('date' => date('Y-m-d H:i'));
      $yml['shop'] = array(
         'name'               => cfg::YML_SHOP_NAME,
         'company'            => cfg::YML_SHOP_COMPANY,
         'url'                => 'http://homy.su',
         'email'              => 'ivan@karapuzoff.net',
         'currencies'         => array(
            'currency'        => array(
               '@attributes'  => array(
                  'id'        => cfg::YML_CURRENCY_ID,
                  'rate'      => 1
               )
            )
         ),
      );

      ### 2 - категории магазина
      $categories = null;
      $this->db->sql('SELECT cat_id,name FROM category WHERE lvl > 1');
      $result = $this->db->getResult();
      foreach ($result as $cat) {
         $categories['category'][] = array(
            '@attributes'  => array('id' => $cat['cat_id']),
            '@value'       => $cat['name']
         );
      }
      $yml['shop']['categories'] = $categories;

      ### 3 - условия доставки
      $yml['shop']['delivery-options'] = array(
         'option' => array(
            '@attributes' => array(
               'cost'         => cfg::SHIPPING_COST,
               'days'         => cfg::YML_DELIVERY_DAYS,
               'order-before' => cfg::YML_ORDER_BEFORE
            )
         )
      );

      ### 4 - заказ на маркете
      $yml['shop']['cpa'] = array(
         '@value' => cfg::YML_CPA
      );

      ### 5 - товарные предложения
      $this->db->sql('
         SELECT
            catalog.1c_id,
            catalog.cat_id,
            category.singular AS singular,
            brands.brand_clean AS brand,
            catalog.name,
            catalog.description,
            catalog.exist,
            catalog.stock AS stock,
            catalog.price,
            catalog.cat_id AS category,
            category.shipping_cost AS shipping_cost,
            images.path_big AS picture
         FROM catalog
         LEFT JOIN
            brands
         ON
            catalog.brand_id = brands.brand_id
         LEFT JOIN
            category
         ON
            catalog.cat_id = category.cat_id
         LEFT JOIN
            images
         ON
            catalog.1c_id = images.1c_id AND images.onmain = 1
         WHERE
            catalog.exist = 1 AND
            catalog.price != 0 AND
            catalog.cat_id != 0 AND
            catalog.1c_id NOT IN ('.$blacklist.')
         GROUP BY catalog.1c_id
      ');
      $result = $this->db->getResult();
      //new dBug($offer);

      $offers = null;
      foreach ($result as $offer) {
         // рассчитываем стоимость доставки позиции
         if ($offer['shipping_cost'] == null) {
            $shipping_cost = ($offer['price'] < cfg::SHIPPING_STEP ? cfg::SHIPPING_COST : cfg::SHIPPING_COST_ST);
         } else {
            $shipping_cost = $offer['shipping_cost'];
         }

         $offers['offer'][$offer['1c_id']] = array(
            '@attributes'           => array(
               'id'                 => $offer['1c_id'],
               'available'          => 'true',
               'bid'                => cfg::YML_BID,
               'cbid'               => cfg::YML_CBID
            ),
            'name'                  => preg_replace("/[\r\n]+/", "",mb_strtoupper($offer['brand']) . ' ' . $offer['name']),
            'vendor'                => mb_strtoupper($offer['brand']),
            'model'                 => preg_replace("/[\r\n]+/", "",$offer['name']),
            //'vendorCode'            => $offer['name'],
            'url'                   => 'http://homy.su'.master::prodURL($offer['1c_id'],$offer['brand'] . ' ' . $offer['name']),
            'price'                 => $offer['price'],
            'currencyId'            => cfg::YML_CURRENCY_ID,
            'categoryId'            => $offer['category'],
            'picture'               => 'http://homy.su' . cfg::IMG_DIR . $offer['picture'],
            'sales_notes'           => cfg::YML_SALES_NOTES,
            'pickup'                => cfg::YML_PICKUP,
            'delivery'              => cfg::YML_DELIVERY,
            'delivery-options'      => array(
               'option'             => array(
                  '@attributes'     => array(
                     'cost'         => $shipping_cost,
                     'days'         => ($offer['exist'] == 1 ? cfg::YML_DELIVERY_DAYS : '')
                  )
               )
            ),
            'outlets'               => array(
               'outlet'             => array(
                  '@attributes'     => array(
                     'id'           => cfg::YML_OUTLET_ID,
                     'instock'      => $offer['stock']
                  )
               )
            ),/**/
            //'typePrefix'            => $offer['singular'],
            'description'           => ($offer['description'] != 'NULL'
                                          ? htmlspecialchars(preg_replace("/[\r\n]+/", "",$offer['description']))
                                          : htmlspecialchars(preg_replace("/[\r\n]+/", "",$offer['singular'].' '.mb_strtoupper($offer['brand']).' '.$offer['name']))
                                       ),
            'manufacturer_warranty' => 'true'
         );

         // если стоимость доставки разниться с общими условиями - указываем ее явно
         if ($shipping_cost == cfg::SHIPPING_COST)
            unset($offers['offer'][$offer['1c_id']]['delivery-options']);
      }
      $yml['shop']['offers'] = $offers;

      ### END - формирование XML
      $xml = yml::createXML(cfg::YML_ROOT_NODE, $yml);

      header("Content-type: text/xml");
      echo $xml->saveXML();
      //file_put_contents(cfg::abs('API_PRICE_PATH').'market.yandex.yml',$xml->saveXML(),LOCK_EX);

      //return true;
   }

   public function geturl() {
      $data = array(
               'api_key'         => cfg::YAPI_KEY,
               'count'           => cfg::YAPI_SEARCH_COUNT,
               'check_spelling'  => cfg::YAPI_SPELL_CHECK,
               'geo_id'          => cfg::YAPI_GEO_ID,
               'text'            => $this->search
            );
      $url = cfg::YAPI_URI.'search'.cfg::YAPI_FORMAT.'?'.http_build_query($data);

      if ($curl = curl_init()) {
         curl_setopt($curl, CURLOPT_URL, $url);
         curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
         $out = curl_exec($curl);
         $info = curl_getinfo($curl);
         curl_close($curl);
      }

      $content = json_decode($out, true);

      foreach ($content['searchResult']['results'] as $item) {
         if (isset($item['offer']))
            break;

         $have = mb_strtolower($item['model']['name']);
         $replace = array('/','');
         $this->search = str_replace($replace,' ',$this->search);
         $this->search = str_replace(' ','|',$this->search);
         if (preg_match('/('.$this->search.')/i', $have)) {
            new dBug($item['model']);
            break;
         }
      }

      //new dBug(master::findKey('model',$content['searchResult']['results']));

      //$content = file_get_contents($url);
      //new dBug($http_response_header);
      //echo cfg::httpResponseHeader($http_response_header);
   }

   public function model() {
      /*$model = str_replace(' ','+',$this->search);
      $url = cfg::YAPI_URI . 'search' . cfg::YAPI_FORMAT . '?api_key=' . cfg::YAPI_KEY . '&count=1&check_spelling=1&text=' . rawurlencode($model);
      //self::send_message($this->step,$url,$this->percent,'warning');

      $content = file_get_contents($url);
      //self::send_message($this->step,$content,$this->percent,'warning');
      $var_dump = json_decode(var_dump($http_response_header),true);

      $content = json_decode($content, true);
      //sleep(cfg::YAPI_WAITTIME);/**/

      $data = array(
               'api_key'         => cfg::YAPI_KEY,
               'count'           => cfg::YAPI_SEARCH_COUNT,
               'check_spelling'  => cfg::YAPI_SPELL_CHECK,
               'geo_id'          => cfg::YAPI_GEO_ID,
               'text'            => $this->search
            );
      $url = cfg::YAPI_URI.'search'.cfg::YAPI_FORMAT.'?'.http_build_query($data);

      // поиск по наименованию
      $content = json_decode(master::curl($url), true);

      foreach ($content['searchResult']['results'] as $item) {
         if (isset($item['offer']))
            break;

         $model = $item['model'];
         $have = mb_strtolower($model['name']);
         $replace = array('/','');
         $this->search = str_replace($replace,' ',$this->search);
         $this->search = str_replace(' ','|',$this->search);
         if (preg_match('/('.$this->search.')/i', $have)) {
            // поиск в каталоге Яндекс.Маркета по идентификатору модели
            $url = cfg::YAPI_URI.'model/'.$model['id'].cfg::YAPI_FORMAT.'?api_key='.cfg::YAPI_KEY;
            $main = json_decode(master::curl($url), true);
            $main = $main['model'];

            // поиск подробных характеристик модели
            $url = cfg::YAPI_URI.'model/'.$model['id'].'/details'.cfg::YAPI_FORMAT.'?api_key='.cfg::YAPI_KEY;
            $prop = json_decode(master::curl($url), true);

            $return = array($model,$main,$prop);
            new dBug($return);

            break;
         }
      }






      /*
      if ($model = master::findKey('model',$content['searchResult']['results'])) {
         // основные характеристики и фото
         $url_model = cfg::YAPI_URI . 'model/' . $model['id'] . cfg::YAPI_FORMAT . '?api_key=' . cfg::YAPI_KEY;
         $main = file_get_contents($url_model);
         $main = json_decode($main, true);
         //sleep(cfg::YAPI_WAITTIME);
         $main = $main['model'];

         // подробные характеристики
         $url_model = cfg::YAPI_URI . 'model/' . $model['id'] . '/details' . cfg::YAPI_FORMAT . '?api_key=' . cfg::YAPI_KEY;
         $prop = file_get_contents($url_model);
         $prop = json_decode($prop, true);
         //sleep(cfg::YAPI_WAITTIME);

         $return = array($model,$main,$prop);

         new dBug($return);
      }/**/
   }

   public function yml() {
      //header("Content-Type: text/xml");

      $blacklist = '56197,29927,29921,26557,54154,43732,45640,31229,31230,45244,45760,42094,51808,45428,45239,56713,45859,40014,28589';

      ### 1 - заголовки и информацио о магазине
      $yml['@attributes'] = array('date' => date('Y-m-d H:i'));
      $yml['shop'] = array(
         'name'               => cfg::YML_SHOP_NAME,
         'company'            => cfg::YML_SHOP_COMPANY,
         'url'                => 'http://homy.su',
         'email'              => 'ivan@karapuzoff.net',
         'currencies'         => array(
            'currency'        => array(
               '@attributes'  => array(
                  'id'        => cfg::YML_CURRENCY_ID,
                  'rate'      => 1
               )
            )
         ),
      );

      ### 2 - категории магазина
      $categories = null;
      $this->db->sql('SELECT cat_id,name FROM category WHERE lvl > 1');
      $result = $this->db->getResult();
      foreach ($result as $cat) {
         $categories['category'][] = array(
            '@attributes'  => array('id' => $cat['cat_id']),
            '@value'       => $cat['name']
         );
      }
      $yml['shop']['categories'] = $categories;

      ### 3 - условия доставки
      $yml['shop']['delivery-options'] = array(
         'option' => array(
            '@attributes' => array(
               'cost'         => cfg::SHIPPING_COST,
               'days'         => cfg::YML_DELIVERY_DAYS,
               'order-before' => cfg::YML_ORDER_BEFORE
            )
         )
      );

      ### 4 - заказ на маркете
      $yml['shop']['cpa'] = array(
         '@value' => cfg::YML_CPA
      );

      ### 5 - товарные предложения
      $this->db->sql('
         SELECT
            catalog.1c_id,
            catalog.cat_id,
            category.singular AS singular,
            brands.brand_clean AS brand,
            catalog.name,
            catalog.description,
            catalog.exist,
            catalog.stock AS stock,
            catalog.price,
            catalog.cat_id AS category,
            images.path_big AS picture
         FROM catalog
         LEFT JOIN
            brands
         ON
            catalog.brand_id = brands.brand_id
         LEFT JOIN
            category
         ON
            catalog.cat_id = category.cat_id
         LEFT JOIN
            images
         ON
            catalog.1c_id = images.1c_id AND images.onmain = 1
         WHERE
            catalog.exist = 1 AND
            catalog.price != 0 AND
            catalog.cat_id != 0 AND
            catalog.1c_id NOT IN ('.$blacklist.')
         GROUP BY catalog.1c_id
      ');
      $result = $this->db->getResult();
      //new dBug($offer);

      $offers = null;
      foreach ($result as $offer) {
         $offers['offer'][] = array(
            '@attributes'           => array(
               'id'                 => $offer['1c_id'],
               'available'          => 'true',
               'bid'                => cfg::YML_BID,
               'cbid'               => cfg::YML_CBID
            ),
            'name'                  => preg_replace("/[\r\n]+/", "",mb_strtoupper($offer['brand']) . ' ' . $offer['name']),
            'vendor'                => mb_strtoupper($offer['brand']),
            'model'                 => preg_replace("/[\r\n]+/", "",$offer['name']),
            //'vendorCode'            => $offer['name'],
            'url'                   => 'http://homy.su'.master::prodURL($offer['1c_id'],$offer['brand'] . ' ' . $offer['name']),
            'price'                 => $offer['price'],
            'currencyId'            => cfg::YML_CURRENCY_ID,
            'categoryId'            => $offer['category'],
            'picture'               => 'http://homy.su' . cfg::IMG_DIR . $offer['picture'],
            'sales_notes'           => cfg::YML_SALES_NOTES,
            'pickup'                => cfg::YML_PICKUP,
            'delivery'              => cfg::YML_DELIVERY,
            'delivery-options'      => array(
               'option'             => array(
                  '@attributes'     => array(
                     'cost'         => ($offer['price'] < cfg::SHIPPING_STEP ? cfg::SHIPPING_COST : cfg::SHIPPING_COST_ST),
                     'days'         => ($offer['exist'] == 1 ? cfg::YML_DELIVERY_DAYS : '')
                  )
               )
            ),
            'outlets'               => array(
               'outlet'             => array(
                  '@attributes'     => array(
                     'id'           => cfg::YML_OUTLET_ID,
                     'instock'      => $offer['stock']
                  )
               )
            ),/**/
            //'typePrefix'            => $offer['singular'],
            'description'           => ($offer['description'] != 'NULL'
                                          ? htmlspecialchars(preg_replace("/[\r\n]+/", "",$offer['description']))
                                          : htmlspecialchars(preg_replace("/[\r\n]+/", "",$offer['singular'].' '.mb_strtoupper($offer['brand']).' '.$offer['name']))
                                       ),
            'manufacturer_warranty' => 'true'
         );
      }
      $yml['shop']['offers'] = $offers;

      ### END - формирование XML
      $xml = yml::createXML(cfg::YML_ROOT_NODE, $yml);
      file_put_contents(cfg::abs('API_PRICE_PATH').'market.yandex.yml',$xml->saveXML(),LOCK_EX);

      exit;
   }

}
